#! /bin/bash
# * BASE_URL
# * PATH_URL
# * FILE_JSON_BODY
# * OUT_FILE
# + GREP_VALUE
source colors.sh


#BASE_URL:PATH_URL
FULL_URL=$BASE_URL$PATH_URL

export NOW=""
#export NOW=$(date +%H%M%S)
export FNOW=$(date +%y%m%d)
#echo "[$NOW] "$FULL_URL | grep $FULL_URL --color
export FULL_FILE=$OUT_FILE"_"$FNOW.json
curl  $CURL_OPTS -X PUT $FULL_URL -d @$FILE_JSON_BODY \
    --header "Content-Type: application/json" > $FULL_FILE
export FILE_VALUE=$(cat $FULL_FILE | grep --color $GREP_VALUE)
echo  $PATH_URL"<put> "$FILE_VALUE | grep --color "$PATH_URL.*>"
#printf "${RED}"$PATH_URL"${LIGHT_GRAY}<put> ${NC}"$FILE_VALUE
