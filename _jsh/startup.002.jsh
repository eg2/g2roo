/set mode me normal -command
/set feedback me
/set prompt me "-> " ">> "
/set format me display ".........................................\n[{type}] {value}\n.........................................\n\n"
/set truncation me 80
/set truncation me 1000 expression,varvalue
