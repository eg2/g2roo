/set mode me normal -command
/set feedback me
/set prompt me "-> " ">> "
/set format me display "\n[{type}] {value}\n\n"
/set truncation me 5000
/set truncation me 5000 expression,varvalue
