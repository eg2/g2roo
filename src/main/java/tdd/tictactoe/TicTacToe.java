package tdd.tictactoe;

public class TicTacToe {
    private static char EMPTY = '\0';
    private static final int SIZE = 3;

    private Character[][] board =
            {{EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY}};

    private char lastPlayer = EMPTY;

    public String play(int x, int y) {
        checkAxis(x);
        checkAxis(y);
        lastPlayer = nextPlayer();
        setBox(x, y, lastPlayer);
        if (isWin(x, y)) {
            return lastPlayer + " is the winner";
        } else if (isDraw()) {
            return "The result is draw";
        } else {
            return "No winner";
        }
    }

    public char nextPlayer() {
        if (lastPlayer == 'X') {
            return '0';
        }
        return 'X';
    }

    private void checkAxis(int axis) {
        if (axis < 1 || axis > 3) {
            throw new RuntimeException("X is outside board");
        }
    }

    private void setBox(int x, int y, char mark) {
        if (board[x - 1][y - 1] != EMPTY) {
            throw new RuntimeException("Box is occupied");
        }
        board[x - 1][y - 1] = mark;
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * SIZE;
        char horizontal, vertical, diagonalLeft, diagonalRight;
        horizontal = vertical = diagonalLeft = diagonalRight = EMPTY;

        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
            diagonalLeft += board[i][i];
            diagonalRight += board[i][SIZE - i - 1];
        }
        System.out.print((char)lastPlayer+"["+x+":"+y+"]");
        System.out.print(" {"+(int)lastPlayer+":"+((int)lastPlayer)*2+"|"+((int)lastPlayer)*SIZE+"} ");
        System.out.println(" {"+(int)horizontal+"|"+(int)vertical+"|"+(int)diagonalLeft+"|"+(int)diagonalRight+"} ");
        if (horizontal == playerTotal
                || vertical == playerTotal
                || diagonalLeft == playerTotal
                || diagonalRight == playerTotal) {
            return true;
        }
        return false;
    }

    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    public String toString() {
        String result = "\n";
        result += "---------\n";
        for (int i = 0; i < 3; i++) {
            result += "| "+ board[0][i]+" "+board[1][i]+" "+board[2][i]+" |\n";
        }
        result += "---------\n";
        return result.replaceAll("\0"," ");
    }

}
