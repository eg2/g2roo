package g2.web.rest.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import g2.domain.RoomRepository;
import g2.domain.Room;

@RestController
public class AdminRoomController {

    @Autowired
    private RoomRepository roomRepository;

    @RequestMapping("/api/admin/rooms/{code}")
    public List<Room> getRoom(@PathVariable("code") String code) {
        return roomRepository.findByCode(code);
    }

    @RequestMapping("/api/admin/rooms")
    public List<Room> getRooms() {
        return roomRepository.findAll();
    }


    @PostMapping("/api/admin/rooms")
    public ResponseEntity<String> addRoom(@RequestBody Room room) {
        roomRepository.save(room);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/api/admin/rooms")
    public ResponseEntity<String> updateRoom(@RequestBody Room room) {
        List<Room> rooms = roomRepository.findByCode(room.code);
        if (rooms.size() == 1) {
            Room currentRoom = rooms.get(0);
            room.id = currentRoom.id;
            roomRepository.save(room);
            //currentRoom.serverHost = room.serverHost;
            //currentRoom.serverPort = room.serverPort;
            //currentRoom.gameMode = room.gameMode;
            //currentRoom.build = room.build;
            //roomRepository.save(current);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/api/admin/rooms/{code}")
    public ResponseEntity<String> deleteRoom(@PathVariable String code
            , @RequestParam(value = "all") Optional<String> oDelAll) {
     //   List<Room> rooms = roomRepository.findByCode(code);
        //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        //System.out.println(oDelAll);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
      //  if (delAll.isPresent() && delAll.get()) {
      //      System.out.println(">>DELETE ALL>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      //      return new ResponseEntity<>(HttpStatus.ACCEPTED);
      //  } else if (rooms.size() == 1) {
      //      System.out.println(">>DELETE ONE>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      //      // Room room = rooms.get(0);
      //      // roomRepository.delete(room);
      //      return new ResponseEntity<>(HttpStatus.ACCEPTED);
      //  }
      //  return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
