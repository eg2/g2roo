package g2.web.rest.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import g2.domain.RoomRepository;
import g2.domain.Room;

@RestController
public class ServerRoomController {

    @Autowired
    private RoomRepository roomRepository;

    @RequestMapping("/api/server/rooms/{code}")
    public List<Room> getRoom(@PathVariable("code") String code) {
        return roomRepository.findByCode(code);
    }

    @RequestMapping("/api/server/rooms")
    public List<Room> getRooms() {
        return roomRepository.findAll();
    }

}
