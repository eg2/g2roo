package g2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Room {

    @Id
    public String id;
    public String code;

    public String serverHost;
    public String serverPort;
    public String serverIPAddress;
    public String gameMode; // FreeFolAll
    public String build; // 11_034234.13

    @Override
    public String toString() {
        return "Ro0m{" +
            ", code='" + code + '\'' +
            '}';
    }
}
