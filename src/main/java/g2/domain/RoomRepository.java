package g2.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface RoomRepository extends MongoRepository<Room, String> {

    public List<Room> findByCode(String code);

    /* @TODO  findOneByCode(String code); */
}
