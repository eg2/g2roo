package g2.domain;

import org.springframework.data.annotation.Id;

public class Player {


    public String playerId;

    public Player(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public String toString() {
        return "Player{" +
            "playerId='" + playerId + '\'' +
            '}';
    }


}
