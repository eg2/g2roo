package tdd.tictactoe;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class TicTacToeTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();
    private TicTacToe tictactoe;

    @Before
    public final void before() {
        tictactoe = new TicTacToe();
    }

    @Test
    public void whenXOutsideTopBoardThenRuntimeException() {
        exception.expect(RuntimeException.class);
        tictactoe.play(0, 2);
    }

    @Test
    public void whenXOutsideBottomBoardThenRuntimeException() {
        exception.expect(RuntimeException.class);
        tictactoe.play(4, 2);
    }

    @Test
    public void whenYOutsideTopBoardThenRuntimeException() {
        exception.expect(RuntimeException.class);
        tictactoe.play(2, 0);
    }

    @Test
    public void whenYOutsideBottomBoardThenRuntimeException() {
        exception.expect(RuntimeException.class);
        tictactoe.play(2, 4);
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        tictactoe.play(1, 1);
        tictactoe.play(2, 1);
        exception.expect(RuntimeException.class);
        tictactoe.play(2, 1);
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertEquals('X' as char, tictactoe.nextPlayer());
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThen0() {
        tictactoe.play(1, 1);
        assertEquals('0' as char, tictactoe.nextPlayer());
    }

    @Test
    public void whenPlayThenNoWinner() {
        String actual = tictactoe.play(1, 1);
        assertEquals("No winner", actual);
        print(tictactoe);
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        tictactoe.play(1, 1); // X
        tictactoe.play(1, 2); // 0
        tictactoe.play(2, 1); // X
        tictactoe.play(2, 2); // 0
        String actual = tictactoe.play(3, 1); // X
        assertEquals("X is the winner", actual);
        print(tictactoe);
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        tictactoe.play(1, 1); // X
        tictactoe.play(2, 1); // 0
        tictactoe.play(1, 2); // X
        tictactoe.play(2, 2); // 0
        String actual = tictactoe.play(1, 3); // X
        assertEquals("X is the winner", actual);
        print(tictactoe);
    }

    @Test
    public void whenPlayAndLeftDiagonalLineThenWinner() {
        tictactoe.play(1, 1); // X
        tictactoe.play(2, 1); // 0
        tictactoe.play(2, 2); // X
        tictactoe.play(2, 3); // 0
        String actual = tictactoe.play(3, 3); // X
        assertEquals("X is the winner", actual);
        print(tictactoe);
    }

    @Test
    public void whenPlayAndRightDiagonalLineThenWinner() {
        tictactoe.play(3, 1); // X
        tictactoe.play(2, 1); // 0
        tictactoe.play(2, 2); // X
        tictactoe.play(1, 2); // 0
        String actual = tictactoe.play(1, 3); // X
        assertEquals("X is the winner", actual);
        print(tictactoe);
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        tictactoe.play(1, 1);
        tictactoe.play(1, 2);
        tictactoe.play(1, 3);
        tictactoe.play(2, 1);
        tictactoe.play(2, 3);
        tictactoe.play(2, 2);
        tictactoe.play(3, 1);
        tictactoe.play(3, 3);
        String actual = tictactoe.play(3, 2);
        assertEquals("The result is draw", actual);
    }

}

/*
public class TicTacToeSpec extends spock.lang.Specification {

    // @ board_boundaries
    // @ Error conditions for outside placed
    def "If a place is placed outside the X axis, then RuntimeException is thrown" () {
        given: "a tictactoe game"
        def ticTacToe = new TicTacToe();
        when: "a piece is placed anywhere outside the x axis"
        ticTacToe.play(5, 2);
        then: "a RuntimeException is thrown"
        RuntimeException e = thrown();
        e.getMessage() == "X is outside board";
    }

    def "If a place is placed outside the Y axis, then RuntimeException is thrown" () {
        given: "a tictactoe game"
        def ticTacToe = new TicTacToe();
        when: "a piece is placed anywhere outside the y axis"
        ticTacToe.play(2, 5);
        then: "a RuntimeException is thrown"
        RuntimeException e = thrown();
        e.getMessage() == "Y is outside board";
    }

    def "If a place is placed on an occupied space, then RuntimeException is thrown" () {
        given: "a tictactoe game"
        def ticTacToe = new TicTacToe();
        and: "a piece placed in 2:1 space"
        ticTacToe.play(2, 1);
        when: "a piece is placed anywhere outside the y axis"
        ticTacToe.play(2, 1);
        then: "a RuntimeException is thrown"
//        RuntimeException e = thrown();
        //e.getMessage() == "Y is outside board";
    }

}
*/
