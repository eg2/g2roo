package wuu.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Adder {
    public int add(int a, int b) {
        return a + b;
    }
}

public class AdderTest {
    @Test
    public void simpleTest() {
        Adder adder = new Adder();
        assertEquals("1 + 1 is 2", 2, adder.add(1, 1));
    }
}

class AdderSpec extends spock.lang.Specification {

def "Adding two numbers to return the sum" () {
    given: "a new Adder class is created"
    def adder = new Adder();
    when: "adding 1 and 1"
    def sum = adder.add(1, 1);
    then: "1 plus 1 is 2"
    sum == 2;
}

def "Order of numbers does not matter" () {
    given: "a new Adder class is created"
    def adder = new Adder();
    when: true
    then: "2 plus 3 is 5"
    adder.add(2, 3) == 5;
    and: "3 plus 2 is also 5"
    adder.add(3, 2) == 5;
}

}
