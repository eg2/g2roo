Test Cases
==========
# Piece Placed [boundaries and unoccuped boxes]#
1. [RA000] A piece can be placed on any unoccuped box of the board.
    101. [RA010] A piece are placed withing the board's boundaries.
    * [TA010:001] When a piece is placed anywhere ouside the x axis, then RuntimeException is thrown.
    * [TA010:002] When a piece is placed anywhere ouside the y axis, then RuntimeException is thrown.
    102. [RA020] A piece are placed only unoccuped box.
    * [TA020:003] When a piece is placed on an occupied box, then RuntimeException is thrown.

# Player Turns [first and last turn how played] #
# In order to track who should play next, we need to store who played last.
2. [RB000] There should be a way to find out which player should play next.
    201. [RB010] A piece for the first turns was played by X.
    * [TB000:004] The first turn should be played by played X.
    202. [RB020] A piece in last turns was played by opponent player.
    * [TB000:005] If the last turn was played by X, then the next turn should be played by 0.
    * [TB000:006] If the last turn was played by 0, then the next turn should be played by X.

# Winning Rules #
# Check all the possibles winning combinations and, if one of them is fulfilled, declare a winner. #
3. [BC000] A player wins by being the first to connect a line of friendly.
    * [TC000:007] If no winning condition is fulfilled, then there is no winner.
    * {TC000:008] The player wins when the whole horizontal line is occupied by his pieces.
    * {TC000:009] The player wins when the whole vertical line is occupied by his pieces.
    * {TC000:010] The player wins when the whole diagonal line is occupied by his pieces.

# Draw Result #
# How to tackle the draw result.
4. [RD000] The result is a draw when all the boxes are filled.
    * [TD000:011] When all boxes are filled then draw.


Notes
=====
# 1 Piece Placed #
- We know that pieces are placed within the board's boundaries.
- We should make sure that they can be placed only on occupied box.
- We know that pieces are placed only an unoccuped boxes.
- We checking, whether a place that was played is occupied and, if it is not, we're changing the array entry value from empty (\0) to occupied (X). Keep in mind that we're still not storing who played (X or 0).

# 2 Player Turns #
- There's no real need to check whether it really is the player's first turn or not.
  As it stands, this test can be fulfilled by always returning X.
- In order to track who should play next, we need to store who played last.

# 3 Winning Rules #
- We should check all the possible winning combinations and, if one of them is fulfilled, declare a winner.
  To check whetter a line of friendly pieces is connected, we should verify horizontal, vertical, and diagonal lines.
- We need to check whether any horizontal line is filled by the same mark as the current player.
  Until this moment, we didn't care what was put to the board array.
  Now, we need to introduce not only which board boxes are empty, but also which player played them.
