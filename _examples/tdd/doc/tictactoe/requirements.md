Requirements
============
# Piece Placed #
# Board Boundaries and Unccuped boxes #
1. [RA000] A piece can be placed on any empty space of the board.
    101. [RA010] A piece are placed withing the board's boundaries.
    102. [RA020] A piece are placed only unoccuped spaces.

# Player Turns #
2. [RB000] There should be a way to find out which player should play next.
    201. [RB010] A piece for the first turns was played by X.
    201. [RB010] A piece for the first turns was played by X.
    202. [RB020] A piece in last turns was played by opponent player.

# Winning Rules #
# Check all the possibles winning combinations and, if one of them is fulfilled, declare a winner. #
3. [RC000] A player wins by being the first to connect a line of friendly.

# Draw Result #
# How to tackle the draw result.
4. [RD000] The result is a draw when all the boxes are filled.



Notes
=====
# Board Boundaries and Unccuped boxes #
* Empty.
# Player Turns #
* The specification of which player is about to play his turn.
# Winning Rules #
* We should check all the possible winning combinations and, if one of them is fulfilled, declare a winner.
  To check whetter a line of friendly pieces is connected, we should verify horizontal, vertical, and diagonal lines.

