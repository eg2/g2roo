package qb9.command.util.math.number.tools;

import java.lang.Integer;

public class Abs {

    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        if (x < 0) {
            System.out.print(-x);
        } else {
            System.out.print(x);
        }
    }
}
