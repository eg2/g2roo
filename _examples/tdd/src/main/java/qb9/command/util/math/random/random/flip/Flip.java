package qb9.command.util.math.random.flip;

import java.lang.Math;

public class Flip {

    public static void main(String[] args) {
        if (Math.random() < 0.5) {
            System.out.print("Heads");
        } else {
            System.out.print("Tails");
        }
    }
}
