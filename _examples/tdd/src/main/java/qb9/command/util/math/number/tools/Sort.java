package qb9.command.util.math.number.tools;

public class Sort {

    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);
        if (x > y) {
            int t = x;
            x = y;
            y = t;
        }
        System.out.print(x+" "+y);
    }
}
