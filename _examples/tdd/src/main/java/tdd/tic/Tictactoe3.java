package tdd.tic;
import util.color.ColorCodes;

public class Tictactoe3 {
    private static char EMPTY = '\0';
    private static final int SIZE = 12;

    public Character[][] board =
            {{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
            ,{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}};

    public char lastPlayer = EMPTY;

    public String play(int x, int y) {
        checkAxis(x);
        checkAxis(y);
        lastPlayer = nextPlayer();
        setBox(x, y, lastPlayer);
        if (isWin(x, y)) {
            return lastPlayer + " is the winner";
        } else if (isDraw()) {
            return "The result is draw";
        } else {
            return "No winner";
        }
    }

    public char nextPlayer() {
        if (lastPlayer == 'X') {
            return '0';
        }
        return 'X';
    }

    private void checkAxis(int axis) {
        if (axis < 1 || axis > SIZE) {
            throw new RuntimeException("X is outside board");
        }
    }

    private void setBox(int x, int y, char mark) {
        if (board[x - 1][y - 1] != EMPTY) {
            throw new RuntimeException("Box is occupied");
        }
        board[x - 1][y - 1] = mark;
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * SIZE;
        char horizontal, vertical, diagonalLeft, diagonalRight;
        horizontal = vertical = diagonalLeft = diagonalRight = EMPTY;

        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
            diagonalLeft += board[i][i];
            diagonalRight += board[i][SIZE - i - 1];
        }
        System.out.print((char)lastPlayer+"["+x+":"+y+"]");
        System.out.print(" {"+(int)lastPlayer+":"+((int)lastPlayer)*2+"|"+((int)lastPlayer)*SIZE+"} ");
        System.out.println(" {"+(int)horizontal+"|"+(int)vertical+"|"+(int)diagonalLeft+"|"+(int)diagonalRight+"} ");
        if (horizontal == playerTotal
                || vertical == playerTotal
                || diagonalLeft == playerTotal
                || diagonalRight == playerTotal) {
            return true;
        }
        return false;
    }

    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    public String toString() {
        String result = "\n[RC]";
        result += "-------------------------\n";
        for (int i = 0; i < 12; i++) {
            // (char)27+ "[37;40;1m"+ " white on black " +(char)27+"[0m"
            result += "| "+ pb(board[0][i]) + " "+pb(board[1][i])+" "+pb(board[2][i])+ pb(board[3][i])+" "+pb(board[4][i])+" "+pb(board[5][i])+" " + pb(board[6][i])+" "+pb(board[7][i])+" "+pb(board[8][i])+ pb(board[9][i])+" "+pb(board[10][i])+" "+pb(board[11][i])+" |\n";
        }
        result += "-------------------------\n";
        return ColorCodes.parseColors(result.replaceAll("\0"," ")) +(char)27+"[0m";
    }


    public String pb(char x) {
        String str;
        if(x =='X') {
            str = ":black,white:X[RC]";
        } else if (x =='0') {
            str = ":white,black:0[RC]";
        } else {
            str = ""+x;
        }
        return str;
    }
}
