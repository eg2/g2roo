import java.util.* ;

class Customer {

    private String _name;
    private Vector _rentals = new Vector();

    public Customer(String name) {
       _name = name;
    }

    public void addRental(Rental arg) {
       _rentals.addElement(arg);
    }

    public String getName() {
       return _name;
    }

    public Enumeration getRentals() {
       return _rentals.elements();
    }

    public String statement() {
       return new PlainStatement().value(this);
    }

    public String htmlStatement() {
       return new HTMLStatement().value(this);
    }

    public int getTotalFrequentRenterPoints() {
       int result = 0;
       Enumeration rentals = _rentals.elements();
       while (rentals.hasMoreElements()) {
          Rental each = (Rental) rentals.nextElement();
          result += each.getFrequentRenterPoints();
       }
       return result;
       
    }

    public double getTotalCharge() {
       double result = 0;
       Enumeration rentals = _rentals.elements();
       while (rentals.hasMoreElements()) {
          Rental each = (Rental) rentals.nextElement();
          result += each.getCharge();
       }
       return result;
    }

}

class PlainStatement {
    public String value(Customer aCustomer) {
        Enumeration rentals = aCustomer.getRentals();

        String result = headerString(aCustomer);

        while (rentals.hasMoreElements()) {
            Rental each = (Rental) rentals.nextElement();
            //show figures for this rental
            result += eachRentalString(each);
        }

        //add footer lines
        result += footerString(aCustomer);

        return result;
    }

    String headerString(Customer aCustomer) {
       return "Rental Record for " + aCustomer.getName() + "\n";
    }

    String eachRentalString(Rental aRental) {
       return "\t" + aRental.getMovie().getTitle()+ "\t" + 
              String.valueOf(aRental.getCharge()) + "\n" ;
    }

    String footerString(Customer aCustomer) {
       return "Amount owed is " + 
              String.valueOf(aCustomer.getTotalCharge()) + "\n" +
              "You earned " + 
              String.valueOf(aCustomer.getTotalFrequentRenterPoints()) + 
              " frequent renter points"
              ;
    }
}

class HTMLStatement {
    public String value(Customer aCustomer) {
        Enumeration rentals = aCustomer.getRentals();
        String result = headerString(aCustomer);

        while (rentals.hasMoreElements()) {
            Rental each = (Rental) rentals.nextElement();
            //show figures for this rental
            result += eachRentalString(each);
        }

        //add footer lines
        result += footerString(aCustomer);

        return result;
    }

    String headerString(Customer aCustomer) {
        return "<H1>Rental Record for <EM>" +
                        aCustomer.getName() + "</EM></H1><P>\n";
    }

    String eachRentalString(Rental aRental) {
       return aRental.getMovie().getTitle()+ ": " + 
              String.valueOf(aRental.getCharge()) + "<BR>\n" ;
    }

    String footerString(Customer aCustomer) {
        return "<P> You owe <EM>" + 
               String.valueOf(aCustomer.getTotalCharge()) + 
               "</EM><P>\n" +
               "On this rental you earned <EM>" + 
               String.valueOf(aCustomer.getTotalFrequentRenterPoints()) + 
               "</EM> frequent renter points<P>"
               ;
    }
}
