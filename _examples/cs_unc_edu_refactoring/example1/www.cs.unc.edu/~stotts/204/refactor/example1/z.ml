datatype LIB =
 new
 | add of LIB * int
 |cko of LIB * int* string
 | wait of LIB * int*string
;

exception hasNewLib;
 fun has ( new, book) = raise hasNewLib
 | has(add(L,book1),book2) = if book1=book2 then true else has(L,book2)
 | has(cko(L,book1,person), book2) = if book1 = book2 then true
 else has(L, book2)
 | has ( wait(L, book1, person), book2) = if book1 = book2 then true
 else has(L, book2)
;

fun num (new, book) = 0
 | num ( add(L, book1), book2) = if book1 = book2 then num(L, book2)+1
 else num(L, book2)
 | num ( cko(L, book1, person), book2) = if book1 = book2 then num(L,book2)-1
 else num(L, book2)
 | num ( wait(L, book1, person), book2) = num(L, book2)
;

fun avail (new, book) = 0
 | avail ( add(L, book1), book2) = if book1 = book2 then avail(L, book2)+1
 else avail(L, book2)
 | avail ( cko(L, book1, person), book2) = if book1 = book2 then       
 avail(L, book2)-1 else avail(L, book2) 
 | avail (wait(L, book1, person), book2) = if book1 = book2 then 0 else 
 avail(L, book2)
;



exception headpersonNew;
fun headperson ( new, book ) = raise headpersonNew
 | headperson ( add(L, book1), book2) = headperson(L, book2)
 | headperson ( cko(L, book1, person), book2) = headperson(L, book2)
 |headperson (wait(L, book1, person), book2) = headperson(L, book2)
;

fun waitnum ( new, book ) = 0
 | waitnum ( add(L, book1), book2) = if book1 = book2 then waitnum(L, book2)-1
 else waitnum(L, book2)
 | waitnum ( cko(L, book1, person), book2) = waitnum(L, book2)
 | waitnum ( wait(L, book1, person), book2) = if book1 = book2
 then waitnum(L, book2)+1 else waitnum(L, book2)
;



fun cki(new, book, person) = new
 | cki ( add(L, book1), book2, person) = if waitnum(L, book2) > 0 
 then cko(L, book2, headperson(L, book2))
 else cki(L, book2, person)
 | cki ( cko(L, book1, person1), book2, person2) = if waitnum (L, book2) > 0
 then cko(L, book2, headperson(L, book2))
 else cki (L, book2, person2)
 | cki ( wait(L, book1, person1), book2, person2) = if waitnum (L, book2) > 0 
 then cko (L, book2, headperson(L, book2))
 else cki(L, book2, person2)
;

exception nahCkoLib;
fun nah(new, book, person) = new
 | nah ( add(L, book1), book2, person) = if book1 = book2 then L
 else nah(L, book2, person)
 | nah ( cko(L, book1, person1), book2, person2) = if waitnum(L, book2)>0
 then raise nahCkoLib else nah(L, book2, person2)
 |nah (wait(L, book1, person1), book2, person2) = L
;

exception remNewLib;
exception remCkoLib;
exception remWaitLib;
fun take (new, book) = raise remNewLib
 | take ( add(L, book1), book2) = if book1 = book2 then L
 else take(L, book2)
 | take ( cko(L, book1, person), book2) = if avail(L, book2)>0
 then take(L, book2) else raise remCkoLib
 | take ( wait( L, book1, person), book2) = if book1 = book2 then   
 raise remWaitLib else take(L, book2)
;


val l1= add(new,1);
val l2 = add(new, 2);
val l3 = add(new, 3);
val l4= cko(l2,2,"abc");
val l5 = wait(l3, 3, "cd");
val l7 = add(new,7);
val b = has(l1,1);
val d= num(l7,7);
val c = has(l2,3);
val g = avail(l3,3);
