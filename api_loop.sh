#! /bin/bash

export BASE_URL="http://localhost:8080/api/"
export GREP_VALUE="id";
export CURL_OPTS="-s"; #"-vs";


export NOW=$(date +%H:%M:%S)
echo "[$NOW] "$BASE_URL | grep . --color

export PATH_URL="greetings?p=1"; export OUT_FILE="get_greetings"; ./xcurl.sh;
export PATH_URL="greetings/1?p=1"; export OUT_FILE="get_greetings_1"; ./xcurl.sh;
export PATH_URL="greetings"; export OUT_FILE="post_greetings"; FILE_JSON_BODY="greeting_new.json" ./post_curl.sh;
export PATH_URL="greetings"; export OUT_FILE="put_greetings"; FILE_JSON_BODY="greeting_update.json" ./put_curl.sh;
export PATH_URL="greetings/2"; export OUT_FILE="del_greetings_1"; ./del_curl.sh;
